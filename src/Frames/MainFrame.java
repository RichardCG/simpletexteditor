package Frames;

import java.awt.BorderLayout;
import java.awt.ScrollPane;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import Panels.ComboBoxesPanel;

public class MainFrame extends JFrame{

	private JTextArea text_area = new JTextArea(60, 20);
	private ScrollPane scroll_pane = new ScrollPane();
	private ComboBoxesPanel combo_boxes_panel = new ComboBoxesPanel(this.text_area);
	
	public MainFrame() {
		this.initUI();
	}
	
	private void initUI() {
		setLayout(new BorderLayout());
		add(combo_boxes_panel, BorderLayout.NORTH);
		
		scroll_pane.add(text_area);
		
		add(scroll_pane, BorderLayout.CENTER);
		setSize(getToolkit().getScreenSize().width / 2, getToolkit().getScreenSize().height / 2);
		setLocation(getToolkit().getScreenSize().width / 4, getToolkit().getScreenSize().height / 4);
		setTitle("Simple Text Editor");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}
