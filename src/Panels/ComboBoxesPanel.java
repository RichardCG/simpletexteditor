package Panels;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class ComboBoxesPanel extends JPanel{
	
	private String[] system_fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
	private Object[] sizes = {8,10,12,14,16,20,24,36, 48};
	private JComboBox font_box = new JComboBox(system_fonts);
	private JComboBox size_box = new JComboBox(sizes);
	private JTextArea text_area;
	
	public ComboBoxesPanel(JTextArea text_area_panel) {
		this.text_area = text_area_panel;
		font_box.addActionListener(font_listener);
		size_box.addActionListener(size_listener);
		add(font_box);
		add(size_box);
	}
	
	private ActionListener font_listener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			text_area.setFont(new Font((String)font_box.getSelectedItem(),
					Font.PLAIN, (int)size_box.getSelectedItem()));
		}
	};
	
	private ActionListener size_listener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			text_area.setFont(new Font((String)font_box.getSelectedItem(),
					Font.PLAIN, (int)size_box.getSelectedItem()));
		}
	};
	
}
